from AlternatingCharacters import alternatingCharacters

import unittest


class testalternatingCharacters(unittest.TestCase):
    def test_give_string_AAABBB(self):
        result = alternatingCharacters('AAABBB')
        self.assertEqual(result, 4)

    def test_give_string_AAAA(self):
        result = alternatingCharacters('AAAA')
        self.assertEqual(result, 3)
