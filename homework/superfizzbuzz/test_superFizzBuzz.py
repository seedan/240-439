from SuperFizzBuzz import checkFizzorBuzz

import unittest


class testcheckFizzorBuzz(unittest.TestCase):
    def test_give_3(self):
        result = checkFizzorBuzz(3)
        self.assertEqual(result.it_fizzofbuzz(), 'Fizz')

    def test_give_225(self):
        result = checkFizzorBuzz(225)
        self.assertEqual(result.it_fizzofbuzz(), 'FizzFizzBuzzBuzz')

    def test_give_15(self):
        result = checkFizzorBuzz(15)
        self.assertEqual(result.it_fizzofbuzz(), 'FizzBuzz')

    def test_give_25(self):
        result = checkFizzorBuzz(25)
        self.assertEqual(result.it_fizzofbuzz(), 'BuzzBuzz')

    def test_give_1(self):
        result = checkFizzorBuzz(1)
        self.assertEqual(result.it_fizzofbuzz(), 'NoFizzBuzz')

    def test_give_2(self):
        result = checkFizzorBuzz(2)
        self.assertEqual(result.it_fizzofbuzz(), 'NoFizzBuzz')

    def test_give_5(self):
        result = checkFizzorBuzz(5)
        self.assertEqual(result.it_fizzofbuzz(), 'Buzz')

    def test_give_9(self):
        result = checkFizzorBuzz(9)
        self.assertEqual(result.it_fizzofbuzz(), 'FizzFizz')

    def test_give_9000(self):
        result = checkFizzorBuzz(9000)
        self.assertEqual(result.it_fizzofbuzz(), 'FizzFizzBuzzBuzz')

    def test_give_9999(self):
        result = checkFizzorBuzz(9999)
        self.assertEqual(result.it_fizzofbuzz(), 'FizzFizz')

    def test_give_300(self):
        result = checkFizzorBuzz(300)
        self.assertEqual(result.it_fizzofbuzz(), 'FizzBuzz')
