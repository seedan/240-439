class superFizzBuzz:
    def __init__(self, number: int):
        self.number = number


class Fizz(superFizzBuzz):
    def it_fizzofbuzz(self):
        return 'Fizz'


class Buzz(superFizzBuzz):
    def it_fizzofbuzz(self):
        return 'Buzz'


class FizzBuzz(superFizzBuzz):
    def it_fizzofbuzz(self):
        return 'FizzBuzz'


class FizzFizz(superFizzBuzz):
    def it_fizzofbuzz(self):
        return 'FizzFizz'


class BuzzBuzz(superFizzBuzz):
    def it_fizzofbuzz(self):
        return 'BuzzBuzz'


class FizzFizzBuzzBuzz(superFizzBuzz):
    def it_fizzofbuzz(self):
        return 'FizzFizzBuzzBuzz'


class NoFizzBuzz(superFizzBuzz):
    def it_fizzofbuzz(self):
        return 'NoFizzBuzz'


def checkFizzorBuzz(num):
    if num % 9 == 0 and num % 25 == 0:
        return FizzFizzBuzzBuzz(num)
    elif num % 3 == 0 and num % 5 == 0:
        return FizzBuzz(num)
    elif num % 25 == 0:
        return BuzzBuzz(num)
    elif num % 9 == 0:
        return FizzFizz(num)
    elif num % 5 == 0:
        return Buzz(num)
    elif num % 3 == 0:
        return Fizz(num)
    else:
        return NoFizzBuzz(num)
