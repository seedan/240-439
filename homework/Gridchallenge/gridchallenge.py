def gridChallenge(grid):
    for i in range(len(grid)):
        x = [j for j in grid[i]]
        x.sort()
        grid[i] = "".join(x)
    Sorted = "YES"
    for i in range(len(grid)-1):
        for j in range(len(grid[0])):
            print(grid[i][j], grid[i+1][j])
            if grid[i][j] > grid[i+1][j]:
                Sorted = "NO"
                break
    return Sorted
