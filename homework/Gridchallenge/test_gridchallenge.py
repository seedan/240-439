from gridchallenge import gridChallenge

import unittest


class testgridchallenge(unittest.TestCase):
    def test_give_liststring1(self):
        data = ['eabcd', 'fghij', 'olkmn', 'trpqs', 'xywuv']
        result = gridChallenge(data)
        self.assertEqual(result, "YES")

    def test_give_liststring2(self):
        data = ['mpxz', 'abcd', 'wlmf']
        result = gridChallenge(data)
        self.assertEqual(result, "NO")
