from catAndmouse import catAndMouse

import unittest


class testcatAndmouse(unittest.TestCase):
    def test_give_1_5_4(self):
        result = catAndMouse(1, 5, 4)
        self.assertEqual(result, "Cat B")

    def test_give_1_2_1(self):
        result = catAndMouse(1, 2, 1)
        self.assertEqual(result, "Cat A")

    def test_give_1_3_2(self):
        result = catAndMouse(1, 3, 2)
        self.assertEqual(result, "Mouse C")

    def test_give_100_100_100(self):
        result = catAndMouse(100, 100, 100)
        self.assertEqual(result, "Mouse C")

    def test_give_negative_8_7_5(self):
        result = catAndMouse(-8, -7, -5)
        self.assertEqual(result, "Cat B")
